# mediation_multifactor

#### 介绍
中介效应的定义
如果自变量X通过某一变量M对因变量Y产生一定影响，则称M为X和Y的中介变量。研究中介作用的目的是在已知X和Y关系的基础上，探索产生这个关系的内部作用机制。作用关系图如下：
![输入图片说明](https://foruda.gitee.com/images/1697460054814140879/470ed3c2_10837548.png "企业微信20231016-204033@2x.png")

X对Y的总效应分为直接效应（direct effect）和间接效应(indirect effect)，直接效应是指当中介变量（M）固定在某一水平时，自变量X对结局变量Y的效应。间接效应是指自变量X通过中介变量M对结局变量Y施加的影响。按上图所示则c为直接效应，间接效应则为a与b的结合。

 根据Baron和Kenny等人的研究，符合以下标准的变量可以判定为中介变量：

1）暴露水平的变化显著影响中介变量水平的变化（即X与M的关联具有统计学意义）。

2）中介变量水平的变化显著影响结局变量（即M与Y的关联具有统计学意义）。

3）暴露因素水平的变化显著影响结局变量（即X与Y的关联具有统计学意义）。

通常情况下标准1）和标准2）被普遍接受，标准3）未被接收，理由是当直接效应和中介效应的作用方向相反时，X与Y间的关联可能无统计学意义，但不能否认中介效应的存在。

#### 软件架构


#### 使用说明

中介效应的R实现

```
mediation包中的mediate函数
install.packages("mediation")
library(mediation)
data(jobs)
b <- lm(job_seek ~ treat + econ_hard + sex + age, data=jobs)
c <- lm(depress2 ~ treat + job_seek + econ_hard + sex + age, data=jobs)
contcont <- mediate(b, c, sims=50, treat="treat", mediator="job_seek")
summary(contcont)
plot(contcont)
```

GEEmediate包中的GEEmediate函数

```
library(lme4)
data("sleepstudy")
sleepstudy
fit.1 <- lm(Reaction ~ Subject*Days, data = sleepstudy)
summary(fit.1)

library(GEEmediate)
#install.packages("GEEmediate")

## Not run:
SimNormalData <- function(n,beta1.star = 1, p = 0.3, rho =0.4, inter = 0)
{
  beta2 <- (p/rho)*beta1.star
  beta1 <- (1-p)*beta1.star
  XM <- MASS::mvrnorm(n, mu = c(0,0), Sigma = matrix(c(1,rho,rho,1),2,2))
  X <- XM[,1]
  M <- XM[,2]
  beta <- c(inter, beta1, beta2)
  print(beta)
  Y <- cbind(rep(1,n),XM)%*%beta+rnorm(n,0,sd = 1)
  return(data.frame(X = X, M = M, Y = Y))
}
set.seed(314)
df <- SimNormalData(500)
GEEmediate(Y ~ X + M, exposure = "X", mediator = "M", df = df)

```

#### 结果说明

ACME stands for average causal mediation effects.间接因果效应，表示X通过M对Y的效应大小
通过med.sum$d0和med.sum$d0.p可以获得ACME的效应和p值
ADE stands for average direct effects.直接效应，表示X直接对Y的作用大小
通过med.sum$z0和med.sum$z0.p可以获得ADE的效应和p值
Total Effect stands for the total effect (direct + indirect) of the IV on the DV. X对Y的直接和间接作用总和
Prop. Mediated describes the proportion of the effect of the IV on the DV that goes through the mediator. X通过M对Y的作用的比例


#### 实战代码

提供3份代码结果
- mediation_multiFactor
实现多个因子做中介分析如
中介变量M（chao1	dominance	goods_coverage	observed_features	pielou_e	shannon	simpson）
结局变量Y（GDM	BMI	LGA	PROM）
协变量cov （week	sex	GWG	BMI	height	education	register	parity	race	unplanned	unemployment）
自变量X （smoking2	alcohol2	physical2	EPDS2	depression2	PASQ2	sleep2	calorie2	animal2	plant2	vitamin2	beverages2	bean2	folate2	multivit2	VitD2	Ga2	Fe2	DHA2	DF2	probiotics2）
这种多变量分析时进行循环处理

![输入图片说明](mediation_multiFactor/med1.png)

- mediation_speciesPlot
复现文献Gut microbiota regulates chronic ethanol exposure-induced depressive-like behavior through hippocampal NLRP3-mediated neuroinflammation  图6C的LPS产物作为中介因子与物种之间的关系

![输入图片说明](mediation_speciesPlot/medi2.png)

- Mediation analysis Figure 2
复现文献The airway microbiome mediates the interaction between environmental exposure and respiratory health in humans 的Figure2代码（作者提供，学习参考）

![输入图片说明](Mediation%20analysis%20Figure%202/medi3.png)
