while True:
    input_names = input("请输入变量名称，以制表符分隔 (输入 'exit' 退出): ")
    
    if input_names.lower() == 'exit':
        print("退出程序。")
        break
    
    variable_names_list = input_names.split("\t")
    output_r_format = 'c(' + ', '.join(['"' + name + '"' for name in variable_names_list]) + ')'
    print(output_r_format)
